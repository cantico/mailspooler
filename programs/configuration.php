<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';


class mailspooler_configEditor
{
	private function getForm()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		$form = $W->Form()->setLayout($W->VBoxItems()->setVerticalSpacing(1, 'em'));
		$form->setName('configuration');
		$form->setHiddenValue('tg', bab_rp('tg'));
		
		$radioSet = $W->RadioSet()->setName('method');
		$radioSet->addOption('realtime', mailspooler_translate('Send mail immediately'));
		$radioSet->addOption('timed', mailspooler_translate('Send mail periodically every five minutes (experimental)'));
		
		
		$form->addItem(
			$W->VBoxItems(
				$tmpLbl = $W->Label(mailspooler_translate('Send method')),
				$radioSet->setAssociatedLabel($tmpLbl)
			)->setVerticalSpacing(.25, 'em')
		);
		
		$keepSentMailCheckBox = $W->CheckBox();
		$keepSentMailCheckBox->setName('keepSentMail');
		
		$tmpLbl = $W->Label(mailspooler_translate('Keep sent mails'));
		$form->addItem(
			$W->FlowItems(
				$keepSentMailCheckBox->setAssociatedLabel($tmpLbl),
				$tmpLbl
			)->setHorizontalSpacing(.25, 'em')
		);
		
		
		$form->addItem(
			$W->VBoxItems(
				$tmpLbl = $W->Label(mailspooler_translate('Number of failed sent mails the system try to re-send every 30 minutes')),
				$resendEdit = $W->LineEdit()->setName('resendNumber')->setAssociatedLabel($tmpLbl)
			)->setVerticalSpacing(.25, 'em')
		);
		
		$form->addItem(
			$W->SubmitButton()
				->setLabel(mailspooler_translate('Save'))
		);
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/mailspooler/');
		
		$method = $registry->getValue('method', 'realtime');
		$resendNumber = $registry->getValue('resendNumber', '40');
		
		$form->setValues(array('configuration' => array('method' => $method)));
		$resendEdit->setValue($resendNumber);
		
		$keepSentMail =  $registry->getValue('keepSentMail');
		
		$keepSentMailCheckBox->setValue($keepSentMail);
		
		return $form;
	}
	
	
	
	public function display()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		$page = $W->BabPage();

		$page->setTitle(mailspooler_translate('Mail spooler options'));
		
		
		$frame = $W->Frame();
		$frame->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
		
		$frame->addClass('BabLoginMenuBackground');
		$frame->addClass('widget-bordered');
		
		$unsentMailButton = $W->Link(
			mailspooler_translate('Unsent mail list'),
			'?tg=addon/mailspooler/mailspool'
		);
		$unsentMailButton->addClass('icon widget-actionbutton ' . Func_Icons::OBJECTS_EMAIL);
		
		$page->addItem(
			$W->FlowItems(
				$unsentMailButton
			)->addClass(Func_Icons::ICON_LEFT_16)
		);

		$configurationForm = $this->getForm();
		
		$frame->addItem($configurationForm);

		$page->addItem($frame);
		
		$page->displayHtml();
	}
	
	
	
	public function save(Array $configuration)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/mailspooler/');
		
		$registry->setKeyValue('method', $configuration['method']);
		$registry->setKeyValue('resendNumber', $configuration['resendNumber']);
		$registry->setKeyValue('keepSentMail', $configuration['keepSentMail']);
	}
}





if (!bab_isUserAdministrator())
{
	return;
}


/* @var $Icons Func_Icons */
$Icons = bab_Functionality::get('Icons');
$Icons->includeCss();

$page = new mailspooler_configEditor;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();