<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__) . '/../functions.php';

require_once dirname(__FILE__) . '/../set/mail.class.php';

bab_Widgets()->includePhpClass('widget_TableModelView');
bab_Widgets()->includePhpClass('widget_Form');

class mailspooler_MailTableView extends widget_TableModelView
{


	public function addDefaultColumns(mailspooler_MailSet $set)
	{
		bab_functionality::includeOriginal('Icons');
		$this->addClass(Func_Icons::ICON_LEFT_16);
		
		$this->addColumn(
			widget_TableModelViewColumn($set->mail_date, mailspooler_translate('Date'))
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->sent_status, mailspooler_translate('Status'))
		);
		$this->addColumn(
            widget_TableModelViewColumn($set->error_msg, mailspooler_translate('Error message'))
		      ->setVisible(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->mail_subject, mailspooler_translate('Subject'))
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->mail_data, mailspooler_translate('Data'))
		      ->setVisible(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->smtp_trace, mailspooler_translate('SMTP trace'))
		      ->setVisible(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->mail_hash, mailspooler_translate('Hash'))
		      ->setVisible(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->body, mailspooler_translate('Body'))
		      ->setVisible(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->altbody, mailspooler_translate('Alt Body'))
		      ->setVisible(false)
		);
		$this->addColumn(
		    widget_TableModelViewColumn($set->format, mailspooler_translate('Format'))
		      ->setVisible(false)
		);
		$this->addColumn(
			widget_TableModelViewColumn($set->recipients, mailspooler_translate('Recipients'))
		);

	}

	
	

	/**
	 * @param ORM_Record	$record
	 * @param string		$fieldPath
	 * @return Widget_Item
	 */
	protected function computeCellContent(ORM_Record $record, $fieldPath)
	{
		
		$W = bab_Widgets();

 		
		

		switch ($fieldPath) {

		    case 'sent_status':

		        if ($record->sent_status === null) {
		            return $W->Label($record->getStatusText())->addClass('icon', Func_Icons::STATUS_CONTENT_LOADING);
		        }
		        if ($record->sent_status == 0) {
		            return $W->Label($record->getStatusText())->addClass('icon', Func_Icons::STATUS_DIALOG_ERROR)
		            ->setTitle($record->error_msg);
		        }
		        if ($record->sent_status == 1) {
		            return $W->Label($record->getStatusText())->addClass('icon', Func_Icons::ACTIONS_DIALOG_OK);
		        }
		        return $W->Label($record->getStatusText());
		        
		    case 'mail_subject':
		        return $W->Link(
                    $record->mail_subject,
                    mailspooler_Controller()->Mail()->display($record->id)
                );
		        
		    case 'mail_data':
		        return $W->Html($record->mail_data);
		        
		    case 'body':
		        return $W->Html($record->body);
		        
			
		}
		

		return parent::computeCellContent($record, $fieldPath);
	}
	
	
	
	/**
	 * Handle label and input widget merge in one item before adding to the filter form
	 * default is a vertical box layout
	 *
	 * @param Widget_Label 					$label
	 * @param Widget_Displayable_Interface 	$input
	 * @return Widget_Item
	 */
	protected function handleFilterLabel(Widget_Label $label, Widget_Displayable_Interface $input)
	{
	    $W = bab_Widgets();
	    $layout = $W->VBoxItems($label, $input);
	
	    $layout->setSizePolicy('widget-25pc');
//	    $input->addClass('widget-75pc');
	    return $layout;
	}
	
	
	/**
	 * Add the filter fields to the filter form
	 * @param Widget_Form $form
	 *
	 */
	protected function handleFilterFields(Widget_Form $form)
	{
	    $W = bab_Widgets();
	
	    $label = $W->Label(mailspooler_translate('Search'));
	    $input = $W->LineEdit()->setName('_search_');
	    $input->addClass('widget-100pc');
	    $label->setAssociatedWidget($input);
	
	    $form->addItem($this->handleFilterLabel($label, $input));
	    
	    $section = $W->Section(
	        mailspooler_translate('Advanced'),
	        $W->FlowLayout(),
	        7
        );
	    //$section->setSizePolicy('widget-100pc');
	    $section->setFoldable(true, true);
	    parent::handleFilterFields($section);
	    
	    $form->addItem($section);
	}
	
	
	
}




class mailspooler_MailEditor extends Widget_Form
{
    public function __construct(mailspooler_Mail $mail)
    {
        $W = bab_Widgets();
        
        parent::__construct(null, $W->FlowLayout()->setSpacing(2, 'em'));
        $this->setName('mail');
        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setHiddenValue('mail', $mail->id);
        
        $ctrl = mailspooler_Controller()->Mail();
        
        $this->addItem(
            $W->SubmitButton()
            ->setLabel(mailspooler_translate('Send'))
            ->setAction($ctrl->send($mail->id))
            ->setSuccessAction($ctrl->displayList())
            ->setFailedAction($ctrl->display($mail->id))
        );
        
        
        $this->addItem(
            $W->SubmitButton()
            ->setLabel(mailspooler_translate('Delete'))
            ->setConfirmationMessage(mailspooler_translate('Are you sure you want to delete this email?'))
            ->setAction($ctrl->delete($mail->id))
            ->setSuccessAction($ctrl->displayList())
            ->setFailedAction($ctrl->display($mail->id))
        );
    }
}
