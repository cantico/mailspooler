<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/

require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__) . '/set/mail.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/mailincl.php';


class Func_Mailspooler extends bab_functionality
{
	/**
	 * (non-PHPdoc)
	 * @see utilit/bab_functionality#getDescription()
	 * 
	 * @return string
	 */
	public function getDescription()
	{
		return mailspooler_translate('Use the mailspooler mail list to save, get, send emails');
	}
	
	/**
	 * Save an email into mailspooler, if the mail allready exists in spooler, the hash of existing email is returned
	 * 
	 * @param	babMail		$babMail
	 * @return string		MD5 of email
	 */
	public function save(babMail $babMail)
	{
		$fakeEvent = new bab_eventMail;
		$fakeEvent->setMailInfos($babMail);
		
		
		$spooler = new mailspooler_mail;
		$hash = $spooler->recordMail($fakeEvent);
		
		$babMail->hash = $hash;
		
		return $hash;
	}
	
	
	private function getRow($hash)
	{
		global $babDB;
		
		$res = $babDB->db_query("
			SELECT * FROM mailspooler_mail WHERE mail_hash=".$babDB->quote($hash)."
		");
		
		if (0 === $babDB->db_num_rows($res))
		{
			throw new ErrorException(sprintf('The email %d does not exists', $hash));
		}
		
		return $babDB->db_fetch_assoc($res);
	}
	
	
	/**
	 * Get a babMail object initilialized with mail in spooler
	 * 
	 * @param	string	$hash
	 * 
	 * @throws ErrorException
	 * @return babMail
	 */
	public function getMail($hash)
	{
		$arr = $this->getRow($hash);
		$mail_obj = bab_mail();
		
		if (!$mail_obj)
		{
			// not configured
			return $mail_obj;
		}
		
		$spooler = new mailspooler_mail;
		$spooler->initMail($mail_obj, $arr);
		
		return $mail_obj;
	}
	

	
	/**
	 * get status and string
	 * return null for waiting email, true for sent mail, false for failed mail
	 * 
	 * @param	string	$hash		Mail md5
	 * @param	string	&$label		status label set by the method
	 * 
	 * 
	 * @return bool | null
	 */
	public function getStatus($hash, &$label)
	{
		try {
			$arr = $this->getRow($hash);
		} catch(ErrorException $e)
		{
			// if not in spooler
			$label = mailspooler_translate('Sent');
			return true;
		}
		
		if (null === $arr['sent_status'])
		{
			$label = mailspooler_translate('Wait for sending');
			return null;
		}
		
		
		if (1 === (int) $arr['sent_status'])
		{
			$label = mailspooler_translate('Sent');
			return true;
		}
		
		if (0 === (int) $arr['sent_status'])
		{
			$label = $arr['error_msg'];
			return false;
		}
		
		$label = '';
		return false;
	}
	
}