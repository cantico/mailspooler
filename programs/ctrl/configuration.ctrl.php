<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/mail.ui.php';
require_once dirname(__FILE__).'/../set/mail.class.php';



/**
 * 
 */
class mailspooler_CtrlConfiguration extends mailspooler_Controller
{

	
	private function getForm()
	{
		$W = bab_Widgets();
		
		$form = $W->Form(null, $W->VBoxItems()->setVerticalSpacing(1, 'em'));
		$form->setName('configuration');
		$form->setHiddenValue('tg', bab_rp('tg'));
		
		$radioSet = $W->RadioSet()->setName('method');
		$radioSet->addOption('realtime', mailspooler_translate('Send mail immediately'));
		$radioSet->addOption('timed', mailspooler_translate('Send mail periodically every five minutes (experimental)'));
		
		
		$form->addItem(
			$W->VBoxItems(
				$tmpLbl = $W->Label(mailspooler_translate('Send method')),
				$radioSet->setAssociatedLabel($tmpLbl)
			)->setVerticalSpacing(.25, 'em')
		);
		
		$keepSentMailCheckBox = $W->CheckBox();
		$keepSentMailCheckBox->setName('keepSentMail');
		
		$tmpLbl = $W->Label(mailspooler_translate('Keep sent mails'));
		$form->addItem(
			$W->FlowItems(
				$keepSentMailCheckBox->setAssociatedLabel($tmpLbl),
				$tmpLbl
			)->setHorizontalSpacing(.25, 'em')
		);
		
		
		$form->addItem(
			$W->VBoxItems(
				$tmpLbl = $W->Label(mailspooler_translate('Number of failed sent mails the system try to re-send every 30 minutes')),
				$resendEdit = $W->LineEdit()->setName('resendNumber')->setAssociatedLabel($tmpLbl)
			)->setVerticalSpacing(.25, 'em')
		);
		
		$form->addItem(
			$W->SubmitButton()
		        ->setAction(mailspooler_Controller()->Configuration()->save())
                ->setLabel(mailspooler_translate('Save'))
		         
		);
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/mailspooler/');
		
		$method = $registry->getValue('method', 'realtime');
		$resendNumber = $registry->getValue('resendNumber', '40');
		
		$form->setValues(array('configuration' => array('method' => $method)));
		$resendEdit->setValue($resendNumber);
		
		$keepSentMail = (bool) $registry->getValue('keepSentMail');
		
		$keepSentMailCheckBox->setValue($keepSentMail);
		
		return $form;
	}
	
	
	
	public function edit()
	{
	    if(!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
		$W = bab_Widgets();
		$page = $W->BabPage();

		$page->setTitle(mailspooler_translate('Mail spooler configuration'));
		
		
		mailspooler_ContextMenu($page, 'configuration');
		
		$frame = $W->Frame();
		$frame->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
		
		$frame->addClass('BabLoginMenuBackground');
		$frame->addClass('widget-bordered');
		
		$configurationForm = $this->getForm();
		
		$frame->addItem($configurationForm);

		$page->addItem($frame);
		
		return $page;
	}
	
	
	
	/**
	 * 
	 * @param array $configuration
	 * @return boolean
	 */
	public function save($configuration = null)
	{
	    $this->requireSaveMethod();
	    
	    if(!bab_isUserAdministrator()) {
	        throw new bab_AccessException(mailspooler_translate('Access denied'));
	    }
	    
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/mailspooler/');
		
		$registry->setKeyValue('method', $configuration['method']);
		$registry->setKeyValue('resendNumber', $configuration['resendNumber']);
		$registry->setKeyValue('keepSentMail', $configuration['keepSentMail']);
		
		
		$this->addMessage(mailspooler_translate('The configuration has been saved.'));
		return true;
	}
	
}


